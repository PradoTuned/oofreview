﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace OOFReview {
	public partial class ProfilePage : ContentPage {
		public ProfilePage() {
			InitializeComponent();
		}

		async void Button_Clicked(object sender, EventArgs e) {
			if (webView.CanGoBack)
				webView.GoBack();
			else {
				temp.HideProfileLink.Execute(null);
			}
		}

		ProfilePageModel temp { get; set; }

		protected override void OnBindingContextChanged() {
			base.OnBindingContextChanged();

			temp = BindingContext as ProfilePageModel;
		}
	}
}