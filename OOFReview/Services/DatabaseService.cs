﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace OOFReview {
	public class DatabaseService : IDatabaseService {
		private List<Profile> _profiles;

		public DatabaseService() {
			_profiles = InitProfiles();
		}

		private List<Profile> InitProfiles() {
			List<Profile> temp = new List<Profile>() {
				new Profile("T1", "https://google.com", "Beach2", "YERRRR"),
				new Profile("T2", "t2.com"),
				new Profile("T3", "t3.com"),
				new Profile("T4", "t4.com"),
				new Profile("T5", "t5.com"),
				new Profile("T6", "t6.com"),
				new Profile("T7", "t7.com"),
				new Profile("T8", "t8.com"),
			};
			temp[0].Reviews.Add(new Models.Review() { Rating = 5, ReviewBody = "Body", ReviewTitle = "Title" });
			temp[0].Reviews.Add(new Models.Review() { Rating = 4, ReviewBody = "Body 2", ReviewTitle = "Title2" });

			return temp;
		}

		public List<Profile> GetProfiles() {
			return _profiles;
		}

		public Result AddProfile(Profile profileToAdd) {
			if (!_profiles.Contains(profileToAdd)) {
				_profiles.Add(profileToAdd);
				return Result.Successful;
			}
			return Result.Failed;
		}

		public void UpdateProfile(Profile profileToUpdate) {
			Profile temp = _profiles.Where(x => x == profileToUpdate).First();
			temp.MemberwiseClone(temp, profileToUpdate);
		}
	}
}
