﻿using System;
using System.Collections.Generic;

namespace OOFReview {
	public enum Result {
		Successful,
		Failed
	}

	public interface IDatabaseService {
		List<Profile> GetProfiles();

		Result AddProfile(Profile profileToAdd);

		void UpdateProfile(Profile profileToUpdate);
	}
}
