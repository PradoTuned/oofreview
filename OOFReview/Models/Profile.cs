﻿using System.Collections.Generic;
using System.Linq;
using OOFReview.Models;
using PropertyChanged;

namespace OOFReview {
    [AddINotifyPropertyChangedInterface]
    public class Profile {
        public string ImageFile { get; set; }
        public string ProfileName { get; set; }
        public string Location { get; set; }
        public string ProfileLink { get; set; }
        public double Rating { get; set; }
        public List<Review> Reviews { get; set; }

        public Profile() {
            Reviews = new List<Review>();
        }
        public Profile(string profileNameToSet = null, string profileLinkToSet = null, string imageFileToSet = null, string locationToSet = null) {
            ProfileName = profileNameToSet;
            ProfileLink = profileLinkToSet;
            ImageFile = imageFileToSet;
            Location = locationToSet;
            Reviews = new List<Review>();
        }

        public void MemberwiseClone(Profile current, Profile toBeCloned) {
            //TODO: Use reflection and auto get all the properties?
            //foreach (PropertyInfo info in ((PropertyInfo)Profile).Get
            current.ImageFile = toBeCloned.ImageFile;
            current.ProfileName = toBeCloned.ProfileName;
            current.Location = toBeCloned.Location;
            current.ProfileLink = toBeCloned.ProfileLink;
            current.Reviews = toBeCloned.Reviews;

            current = toBeCloned;
        }
    }
}
