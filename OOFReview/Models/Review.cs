﻿using System;
using PropertyChanged;

namespace OOFReview.Models {
	[AddINotifyPropertyChangedInterface]
	public class Review {
		public const double MAX_RATING = 5;
		public const double LOWEST_RATING = 0;

		public string ReviewTitle { get; set; }
		public string ReviewBody { get; set; }
		public double Rating { get; set; } = LOWEST_RATING;

		public Review() { }
	}
}