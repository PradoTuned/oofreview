using System;
using FreshMvvm;
using Xamarin.Forms;
using Plugin.Toasts;
using Plugin.Toasts.Options;
using Xamarin.Forms.Xaml;

namespace OOFReview {
	public partial class App : Application {
		public App() {
			Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjcyMzExQDMxMzgyZTMxMmUzMGpjTXhxUUNEMXhaV1dGZXJNaTRlUkthWkFPbFY1cWdXSjJmcmlGTWkxNGc9");

			InitializeComponent();

			/* 
			 * SUPER IMPORTANT: This registers the first Type, in this case
			 * IDatabaseService into a singleton object of type DatabaseService.
			 * 
			 * This allows for ANY creation of the DatabaseService class to be 
			 * the same "global" object through the use of the IOC (inversion of control?).
			 * 
			 * Makes it super easy to use the now global instance of the DatabaseService class 
			 * via the Register method.
			*/
			FreshIOC.Container.Register<IDatabaseService, DatabaseService>();

			var page = FreshPageModelResolver.ResolvePageModel<ProfileListPageModel>();
			var basicNavContainer = new FreshNavigationContainer(page);
			MainPage = basicNavContainer;
		}

		protected override void OnStart() {
		}

		protected override void OnSleep() {
		}

		protected override void OnResume() {
		}
	}
}
