﻿using System;
using FreshMvvm;
using Plugin.Toasts;
using Xamarin.Forms;

namespace OOFReview {
	public class PTBasePageModel : FreshBasePageModel {
		public PTBasePageModel() { }

		protected void ShowToastNotification(INotificationOptions notificationOptions) {
			var notificator = DependencyService.Get<IToastNotificator>();

			notificator.Notify((INotificationResult result) => {
				System.Diagnostics.Debug.WriteLine("Notification [" + result.Id + "] Result Action: " + result.Action);
			}, notificationOptions);
		}

		protected void ShowToastNotification(string title, string description) {
			var notificationOptions = new NotificationOptions() {
				Title = title,
				Description = description,
				IsClickable = false,
				ClearFromHistory = true,
				AllowTapInNotificationCenter = false
			};

			ShowToastNotification(notificationOptions);
		}

		public Command BackOrCancelCommand => new Command(async () => await CoreMethods.PopPageModel());
	}
}
