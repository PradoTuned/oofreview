﻿using System;
using FreshMvvm;
using OOFReview.Models;
using PropertyChanged;
using Xamarin.Forms;

namespace OOFReview {
	[AddINotifyPropertyChangedInterface]
	public class ReviewAddPageModel : PTBasePageModel {
		#region Properties
		IDatabaseService _databaseService;

		public Profile Profile { get; set; }
		public Review Review { get; set; }
		#endregion

		public ReviewAddPageModel(IDatabaseService databaseService) {
			_databaseService = databaseService;

			Review = new Review();
			this.WhenAny(HandleReviewChanged, o => o.Review);
		}

		#region BasePageModel Overrides
		public override void Init(object initData) {
			if (initData != null) {
				Profile = (Profile)initData;
			} else {
				CoreMethods.DisplayAlert("No profile passed in", "", "OK");
			}
		}
		#endregion

		#region Private Methods
		private void HandleReviewChanged(string obj) {
			//Handle the property changed
			throw new NotImplementedException();
		}
		#endregion

		#region Button Commands
		public Command SaveCommand {
			get {
				return new Command(async () => {
					Profile.Reviews.Add(Review);
					_databaseService.UpdateProfile(Profile);
					ShowToastNotification("Review Successfully Added", $"Your review for {Profile.ProfileName} was successfully added");
					await CoreMethods.PopPageModel(Profile);
				});
			}
		}
		#endregion
	}
}