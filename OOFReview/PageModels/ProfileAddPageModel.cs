﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using FreshMvvm;
using PropertyChanged;
using Xamarin.Forms;
using Plugin.Toasts;
using Plugin.Toasts.Options;
using System.Threading.Tasks;

namespace OOFReview {
	[AddINotifyPropertyChangedInterface]
	public class ProfileAddPageModel : PTBasePageModel {
		#region Properties
		IDatabaseService _databaseService;

		private Profile _profile { get; set; }

		public string ImageFile {
			get => _profile.ImageFile;
			set => _profile.ImageFile = value;
		}

		public string ProfileName {
			get => _profile.ProfileName;
			set => _profile.ProfileName = value;
		}

		public string Location {
			get => _profile.Location;
			set => _profile.Location = value;
		}

		public string ProfileLink {
			get => _profile.ProfileLink;
			set => _profile.ProfileLink = value;
		}
		#endregion

		public ProfileAddPageModel(IDatabaseService databaseService) {
			_databaseService = databaseService;

			this.WhenAny(HandleProfileChanged, o => o._profile);
		}

		#region BasePageModel Overrides
		public override void Init(object initData) {
			if (initData != null) {
				_profile = (Profile)initData;
			} else {
				_profile = new Profile();
			}
		}
		#endregion

		#region Private Methods
		private void HandleProfileChanged(string propertyName) {
			//Handle the property changed
		}
		#endregion

		#region Button Commands
		public Command SaveCommand {
			get {
				return new Command(async () => {
					switch (_databaseService.AddProfile(_profile)) {
						case Result.Failed:
							ShowToastNotification("Profile Creation Failed", $"{ProfileName} could not be added. " +
								$"Please try again later.");
							break;
						default:
							ShowToastNotification("Profile Added Successfully", $"{ProfileName}'s Profile was added successfully");
							break;
					}

					await CoreMethods.PopPageModel(_profile);
				});
			}
		}
		#endregion
	}
}