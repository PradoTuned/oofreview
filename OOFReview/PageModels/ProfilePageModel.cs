using System;
using System.Linq;
using PropertyChanged;
using FreshMvvm;
using Xamarin.Forms;

namespace OOFReview {
	[AddINotifyPropertyChangedInterface]
	public class ProfilePageModel : PTBasePageModel {
		#region Properties
		private Profile _profile;
		public Profile Profile {
			get => _profile;
			set {
				//TODO: Create a method to check if the members of the ProfileClass are the same or not in addition
				// to checking if it's a whole different object. This will handle the properties that do get updated, I hope
				if (value != _profile)
					_profile = value;

				UpdateProperties();
			}
		}

		public string Link { get; set; }
		public bool LinkVisible { get; set; }

		private int _reviewCount;
		public int ReviewCount {
			get { return _reviewCount; }
			private set {
				if (value != _reviewCount)
					_reviewCount = value;
			}
		}

		private string _reviewCountString;
		public string ReviewCountString {
			get => _reviewCountString;
			private set {
				if (value != _reviewCountString)
					_reviewCountString = value;
			}//$"{ReviewCount} " + GetReviewString();
		}


		#endregion

		public ProfilePageModel() { }

		#region BasePageModel Overrides
		public override void Init(object initData) {
			if (initData != null) {
				Profile = (Profile)initData;
			} else {
				Profile = new Profile();
			}
		}

		public override void ReverseInit(object returnedData) {
			if (returnedData != null) {
				Profile = (Profile)returnedData;    // When we execute this setter it will refresh the properties
													//(even though it's the same object when popped) 
													//TODO: how do we handle this when we're "editing" a profile?
			}
		}
		#endregion

		#region Private Methods
		private void UpdateProperties() {
			ReviewCount = Profile.Reviews.Count();
			ReviewCountString = $"{ReviewCount} " + GetReviewString();
			Profile.Rating = GetAverageRatings();   //TODO: figure out if this is the correct place to place this? It makes the binding work.
		}

		private double GetAverageRatings() {
			return Profile.Reviews.Count > 0 ? Math.Round(Profile.Reviews.Average(x => x.Rating), 2) : 0;
		}

		private string GetReviewString() {
			return ReviewCount == 1 ? "review" : "reviews";
		}
		#endregion

		#region Button Commands
		public Command ShowReviews {
			get {
				if (ReviewCount > 0) {
					return new Command(async () => {
						await CoreMethods.PushPageModel<ReviewListPageModel>(Profile);
					});
				} else {

					return new Command(async () => {
						//ShowToastNotification("No Reviews To display", "");
						await CoreMethods.DisplayAlert("No reviews to display", "", "OK");
					});
				}
			}
		}

		public Command AddReview {
			get {
				return new Command(async () => {
					await CoreMethods.PushPageModel<ReviewAddPageModel>(Profile);
				});
			}
		}

		public Command ShowProfileLink {
			get {
				return new Command(() => {
					Link = Profile.ProfileLink;
					LinkVisible = true;
				});
			}
		}

		public Command HideProfileLink {
			get {
				return new Command(() => {
					Link = string.Empty;
					LinkVisible = false;
				});
			}
		}
		#endregion
	}
}