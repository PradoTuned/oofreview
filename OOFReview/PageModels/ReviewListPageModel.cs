﻿using System;
using System.Collections.ObjectModel;
using FreshMvvm;
using OOFReview.Models;
using PropertyChanged;
using Xamarin.Forms;

namespace OOFReview {
	[AddINotifyPropertyChangedInterface]
	public class ReviewListPageModel : PTBasePageModel {
		#region Properties
		IDatabaseService _databaseService;

		public ObservableCollection<Review> Reviews { get; set; }
		#endregion

		//Review _selectedReview;
		//public Review SelectedReview {
		//	get { return _selectedReview; }
		//	set {
		//		_selectedReview = value;
		//		if (value != null)
		//			ReviewSelected.Execute(value);
		//	}
		//}

		// For later if we want to edit a review?
		public ReviewListPageModel() {

		}

		#region BasePageModel Overrides
		// Since this function will be seen via a Profile click, should we be
		// passing in a profile and extracting the Reviews? How do we further decouple it?
		public override void Init(object initData) {
			if (initData != null) {
				Reviews = new ObservableCollection<Review>(((Profile)initData).Reviews);
			} else {
				Reviews = new ObservableCollection<Review>(new Profile().Reviews);
			}
		}
		#endregion

		//public Command<Review> ReviewSelected {
		//	get {
		//		return new Command<Review>(async (review) => {
		//			await CoreMethods.PushPageModel<ReviewPageModel>(review);
		//		});
		//	}
		//}
	}
}
