using System;
using System.Collections.ObjectModel;
using PropertyChanged;
using FreshMvvm;
using Xamarin.Forms;
using System.Linq;
using Xamarin.Forms.Internals;
using System.Reactive.Linq;

namespace OOFReview {
	[AddINotifyPropertyChangedInterface]
	public class ProfileListPageModel : FreshBasePageModel {
		#region Properties
		IDatabaseService _databaseService;

		public ObservableCollection<Profile> Profiles { get; set; }

		Profile _selectedProfile;
		public Profile SelectedProfile {
			get { return _selectedProfile; }
			set {
				_selectedProfile = value;
				if (value != null) {
					ProfileSelected.Execute(value);
					// Setting this back to null, ensures the hightlighting on the CollectionView
					// that's bound to this property gets reset so we can re-click the same item again
					// Found from here: https://forums.xamarin.com/discussion/172735/collectionview-unselect-single-vs-multiple
					_selectedProfile = null;
				}
			}
		}

		public ProfileListPageModel(IDatabaseService databaseService) {
			_databaseService = databaseService;
		}

		public override void Init(object initData) {
			Profiles = new ObservableCollection<Profile>(_databaseService.GetProfiles());
		}

		// Updating the Profiles list anytime this page is returned to via a PopPageModel.
		public override void ReverseInit(object returnedData) {
			UpdateProfilesList();
		}

		private void UpdateProfilesList() {
			if (Profiles != null) {
				Profiles.Clear();
				_databaseService.GetProfiles().ForEach(x => Profiles.Add(x));
			}
		}

		public Command AddProfile {
			get {
				return new Command(async () => {
					await CoreMethods.PushPageModel<ProfileAddPageModel>();
				});
			}
		}

		public Command<Profile> ProfileSelected {
			get {
				return new Command<Profile>(async (profile) => {
					await CoreMethods.PushPageModel<ProfilePageModel>(profile);
				});
			}
		}
		#endregion
	}
}